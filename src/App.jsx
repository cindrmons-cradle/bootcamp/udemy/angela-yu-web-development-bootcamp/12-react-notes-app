import React, { useState } from "react";
import Header from "./Components/Header";
import Footer from "./Components/Footer";
import Note from "./Components/Note";
import CreateArea from "./Components/CreateArea";

function App() {
  const [notes, setNotes] = useState([]);

  function createNote(note) {
    setNotes((currentNote) => {
      return [...currentNote, note];
    });
  }

  function deleteNote(noteId) {
    setNotes((currentNote) => currentNote.filter((note) => note.id !== noteId));
  }

  return (
    <div>
      <Header />
      <CreateArea insertNote={createNote} />
      {notes.map((note) => (
        <Note
          key={note.id}
          id={note.id}
          title={note.title}
          content={note.content}
          deleteThisNote={deleteNote}
        />
      ))}
      <Footer />
    </div>
  );
}

export default App;
