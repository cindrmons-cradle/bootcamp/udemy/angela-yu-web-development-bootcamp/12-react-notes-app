import React from "react";

// computed
function getCurrentYear() {
  return new Date().getFullYear();
}

// template
function Footer() {
  return (
    <footer>
      <p>Copyright ⓒ lololol {getCurrentYear()}</p>
    </footer>
  );
}

export default Footer;
