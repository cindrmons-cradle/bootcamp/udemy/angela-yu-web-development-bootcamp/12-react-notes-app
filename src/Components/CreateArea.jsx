import React, { useState } from "react";
import { v4 as UUIDv4 } from "uuid";

function CreateArea(props) {
  const [note, setNote] = useState({
    id: "",
    title: "",
    content: "",
  });

  function inputNotes(e) {
    const { name, value } = e.target;

    setNote((currentNote) => {
      return {
        ...currentNote,
        id: UUIDv4(),
        [name]: value,
      };
    });
  }

  function submitNote(e) {
    e.preventDefault();
    props.insertNote(note);
    console.log("Submitted");
    setNote({
      id: "",
      title: "",
      content: "",
    });
  }

  return (
    <div>
      <form onSubmit={submitNote}>
        <input
          onChange={inputNotes}
          name="title"
          value={note.title}
          placeholder="Title"
          required
        />
        <textarea
          onChange={inputNotes}
          name="content"
          value={note.content}
          placeholder="Take a note..."
          rows="3"
          required
        />
        <button type="submit">Add</button>
      </form>
    </div>
  );
}

export default CreateArea;
