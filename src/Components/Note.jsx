import React from "react";

function Note(props) {

  function deleteThis() {
    props.deleteThisNote(props.id);
  }

  return (
    <div className="note">
      <h1>{props.title}</h1>
      <p>{props.content}</p>
      <button onClick={deleteThis}>DELETE</button>
    </div>
  );
}

export default Note;
