import React from "react";

function Header() {
  return (
    <header id="header">
      <h1>Keeper App</h1>
    </header>
  );
}

export default Header;
