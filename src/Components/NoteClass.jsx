import React from "react";

class Note extends React.Component {
    constructor(props) {
        return super(props);
    }

    // methods?
    function deleteThis() {
        props.deleteThisNote(props.id);
    }

    // template?
    render() {
        return (
            <div className="note">
                <h1>{props.title}</h1>
                <p>{props.content}</p>
                <button onClick={deleteThis}>DELETE</button>
            </div>
        );
    }
}

export default Note;
